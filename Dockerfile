# use small node image
FROM node:alpine

# install jq for JSON parsing
RUN apk add --update --no-cache jq gettext xmlstarlet coreutils rsync


# install latest sfdx from npm
RUN npm install sfdx-cli --global
RUN sfdx --version
RUN sfdx plugins --core

ENV NODE_TLS_REJECT_UNAUTHORIZED 0

# revert to low privilege user
USER node
